package com.kutylo.view;

import com.kutylo.controller.Controller;
import com.kutylo.controller.ControllerImpl;
import com.kutylo.model.Bank;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();

        menu.put("1", "  1 - parse DOM");
        menu.put("2", "  2 - parse Sax");
        menu.put("3", "  3 - parse Strax");
        menu.put("4", "  4 - sort parse result");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);

    }

    private void  pressButton1() {
        controller.parseDOM();
    }

    private void pressButton2() {
        controller.parseSax();
    }

    private void pressButton3() {
        controller.parseStax();
    }

    private void pressButton4() {
        controller.sortParseResult();
    }


    //-------------------------------------------------------------------------
    /**
     * method print menu in console
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * method for write user choose
     */

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
