package com.kutylo.model;

import com.kutylo.parser.XMLValidator;
import com.kutylo.parser.dom.DOMDocCreator;
import com.kutylo.parser.dom.DOMDocReader;
import com.kutylo.parser.sax.SAXHandler;
import com.kutylo.parser.stax.STAXParser;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParseBank {
    private File xml;
    private File xsd;
    private List<Bank> banks;

    public ParseBank(File xml, File xsd) {
        banks=new ArrayList<>();
        this.xml = xml;
        this.xsd = xsd;
    }

    public List<Bank> sortBanks(){
        if(!banks.isEmpty()){
            banks.sort(new BankComparator());
        }else{
            System.out.println("Please first parse xml file!!");
        }
        return banks;

    }

    public List<Bank> domParser(){

        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();

        if(!XMLValidator.validate(XMLValidator.createSchema(xsd),xml)){
            throw new RuntimeException("validation failed");
        }


        DOMDocReader reader = new DOMDocReader();
        banks=reader.readDoc(doc);
        return banks;
    }

    public List<Bank> saxParser(){
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            Schema schema = XMLValidator.createSchema(xsd);
            saxParserFactory.setSchema(XMLValidator.createSchema(xsd));

            if(!XMLValidator.validate(schema,xml)){
                throw new RuntimeException("Validation failed");
            }
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SAXHandler saxHandler = new SAXHandler();
            saxParser.parse(xml, saxHandler);

            banks = saxHandler.getBanks();
        }catch (SAXException | ParserConfigurationException | IOException ex){
            ex.printStackTrace();
        }
        return banks;

    }

    public List<Bank> staxParser(){
        banks=STAXParser.parseFile(xml);
        return banks;
    }
}
