package com.kutylo.model;

public enum DepositType {
    QUESTION, EXPRESS, CALCULATED, ACCUMULATIVE, SAVING, METALLIC
}
