package com.kutylo;

import com.kutylo.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
