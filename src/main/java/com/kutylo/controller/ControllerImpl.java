package com.kutylo.controller;

import com.kutylo.model.ParseBank;
import com.kutylo.parser.WriteToXml;

import java.io.File;

public class ControllerImpl implements Controller {
    private ParseBank parseBank;
    private File xml=new File("src/main/resources/xml/banksXML.xml");
    private File xsd=new File("src/main/resources/xml/banksXSD.xsd");

    public ControllerImpl(){
        parseBank=new ParseBank(xml,xsd);
    }

    @Override
    public void parseDOM() {
        System.out.println(parseBank.domParser());
    }

    @Override
    public void parseSax() {
        System.out.println(parseBank.saxParser());
    }

    @Override
    public void parseStax() {
        System.out.println(parseBank.staxParser());
    }

    @Override
    public void sortParseResult() {
        System.out.println(parseBank.sortBanks());
        try {
            new WriteToXml().WriteIntoXML(parseBank.sortBanks());
            System.out.println("Result of filtration wrote to new xml file");
        }catch (Exception e){
            System.out.println(e);
        }

    }

}
