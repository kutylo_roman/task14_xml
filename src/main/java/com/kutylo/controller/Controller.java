package com.kutylo.controller;

import com.kutylo.model.Bank;

import java.io.File;
import java.util.List;

public interface Controller {
    void parseDOM();
    void parseSax();
    void parseStax();
    void sortParseResult();
}
