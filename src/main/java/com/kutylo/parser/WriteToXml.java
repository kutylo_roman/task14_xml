package com.kutylo.parser;

import com.kutylo.model.Bank;
import com.kutylo.model.DepositType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class WriteToXml {
    private DocumentBuilderFactory builderFactory;
    private DocumentBuilder documentBuilder;
    private Document document;

    private void createDOMBuilder(){
        builderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = builderFactory.newDocumentBuilder();
        }catch (ParserConfigurationException ex){
            ex.printStackTrace();
        }
    }

    public void WriteIntoXML(List<Bank> banks) throws TransformerException, IOException {
        createDOMBuilder();
        Document doc=documentBuilder.newDocument();

        Element titleElement=doc.createElement("banks");
        doc.createElement("banks");

        for(Bank bank:banks) {

            Element bankNode = doc.createElement("bank");
            bankNode.setAttribute("id",String.valueOf(bank.getBankId()));

            Element nameBank = doc.createElement("name");
            nameBank.appendChild(doc.createTextNode(bank.getName()));
            bankNode.appendChild(nameBank);

            Element country = doc.createElement("country");
            country.appendChild(doc.createTextNode(bank.getCountry()));
            bankNode.appendChild(country);

            Element depositTypes = doc.createElement("deposit_types");
                for (DepositType i:bank.getDepositTypes()){
                    Element depositType = doc.createElement("deposit_type");
                    depositType.appendChild(doc.createTextNode(i.toString()));
                    depositTypes.appendChild(depositType);
                }
            bankNode.appendChild(depositTypes);

            Element depositor = doc.createElement("depositor");
            depositor.appendChild(doc.createTextNode(bank.getDepositor()));
            bankNode.appendChild(depositor);

            Element account_id = doc.createElement("account_id");
            account_id.appendChild(doc.createTextNode(String.valueOf(bank.getAccountId())));
            bankNode.appendChild(account_id);

            Element amount_on_deposit = doc.createElement("amount_on_deposit");
            amount_on_deposit.appendChild(doc.createTextNode(String.valueOf(bank.getAmountOnDeposit())));
            bankNode.appendChild(amount_on_deposit);

            Element profitability = doc.createElement("profitability");
            profitability.appendChild(doc.createTextNode(String.valueOf(bank.getProfitability())));
            bankNode.appendChild(profitability);

            Element time_constraints = doc.createElement("time_constraints");
            time_constraints.appendChild(doc.createTextNode(bank.getDuration().toString()));
            bankNode.appendChild(time_constraints);

            titleElement.appendChild(bankNode);
        }

        doc.appendChild(titleElement);



        Transformer t= TransformerFactory.newInstance().newTransformer();
        t.setOutputProperty(OutputKeys.METHOD, "xml");
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(new DOMSource(doc), new StreamResult(new FileOutputStream("src/main/resources/xml/sortedBanks.xml")));

    }
}
